
public class Einmaleins {

	public static void main(String[] args) {
		System.out.print("    ");
		for (int i = 1; i <= 10; i++) {
			if (i < 10) {
				System.out.print(" ");
			}
			System.out.print("  " + i);
		}
		System.out.print("\n");
		for (int i = 1; i <= 10; i++) {
			if (i < 10) {
				System.out.print(" ");
			}
			System.out.print("  " + i);
			for (int j = 1; j <= 10; j++) {
				int produkt = i * j;
				if (produkt < 100) {
					System.out.print(" ");
				}
				if (produkt < 10) {
					System.out.print(" ");
				}
				System.out.print(" " + produkt);

			}
			System.out.print("\n");

		}

	}

}

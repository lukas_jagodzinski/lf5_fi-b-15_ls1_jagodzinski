import java.util.Scanner;

public class Treppe {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Wie hoch soll die Treppe sein?");
		int h = scanner.nextInt();
		System.out.println("Wie breit sollen die Stufen sein?");
		int b = scanner.nextInt();
		for (int i = 0; i < h; i++) {
			for(int j = 0; j<(h*b-i*b-b); j++) {
				System.out.print(" ");
			}
			for (int j = 0;j < b*(i+1); j++) {
				System.out.print("*");
			}
			System.out.print("\n");	
		}
		scanner.close();

	}

}

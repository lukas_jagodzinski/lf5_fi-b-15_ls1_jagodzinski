import java.util.Scanner;

public class Fakultaet {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = -1;
		int i = 1;
		long fakultaet = 1l;
		do {
			System.out.println("Für welche Zahl (0-20) wollen sie die Fakultät berechnen?");
			n = scanner.nextInt();
			if (n>20) {
				System.out.println("Der eingegebene Wert ist zu groß!");	
			} else if (n<0) {
				System.out.println("Der eingegebene Wert ist zu klein!");	
			}
		} while (n > 20 || n < 0);

		do {
			fakultaet = fakultaet * i;
			i++;
		} while (i <= n);
		System.out.println(n + "! ist: " + fakultaet);
		scanner.close();
	}
}

import java.util.Scanner;

public class Quersumme {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Von welcher Zahl soll die Quersumme berechnet werden?");
		int eingabe = scanner.nextInt();
		int quersumme = 0;
		int zahl = eingabe;
		
		while (zahl >0) {
			int modulo = zahl%10;
			quersumme = quersumme + modulo;
			zahl = (zahl-modulo)/10;
		}
		System.out.println("Die Quersumme von " + eingabe + " ist: " + quersumme);
		scanner.close();
	}

}

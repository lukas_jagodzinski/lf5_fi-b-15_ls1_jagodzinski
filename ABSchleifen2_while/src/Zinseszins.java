import java.util.Scanner;

public class Zinseszins {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Wie ist die Laufzeit des des Sparvertrages in Jahren?");
		int laufzeit = scanner.nextInt();
		System.out.println("Wieviel Kapital wollen sie anlegen?");
		double startkapital = scanner.nextDouble();
		System.out.println("Wie hoch ist der Zinssatz");
		double zinssatz = scanner.nextDouble();
		double kapital = startkapital;
		int jahr = 0;
		while (jahr < laufzeit) {
			kapital = kapital * (100+zinssatz)/100;
			jahr++;
		}
		System.out.printf("\nEingezahltes Kapital: %.2f Euro\n", startkapital);
		System.out.printf("Eingezahltes Kapital: %.2f Euro", kapital);
		scanner.close();
	}
}

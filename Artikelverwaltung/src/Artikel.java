public class Artikel {
    private String bezeichnung;
    private int artikelnummer;
    private double einkaufspreis;
    private double verkaufspreis;
    private int sollBestand;
    private int istBestand;



    public Artikel(String bezeichnung, int artikelnummer, double einkaufspreis, double verkaufspreis, int sollBestand, int istBestand) {
        this.bezeichnung = bezeichnung;
        this.artikelnummer = artikelnummer;
        this.einkaufspreis = einkaufspreis;
        this.verkaufspreis = verkaufspreis;
        this.sollBestand = sollBestand;
        this.istBestand = istBestand;
    }

    public Artikel() {
    }


    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public int getArtikelnummer() {
        return artikelnummer;
    }

    public void setArtikelnummer(int artikelnummer) {
        this.artikelnummer = artikelnummer;
    }

    public double getEinkaufspreis() {
        return einkaufspreis;
    }

    public void setEinkaufspreis(double einkaufspreis) {
        this.einkaufspreis = einkaufspreis;
    }

    public double getVerkaufspreis() {
        return verkaufspreis;
    }

    public void setVerkaufspreis(double verkaufspreis) {
        this.verkaufspreis = verkaufspreis;
    }

    public int getSollBestand() {
        return sollBestand;
    }

    public void setSollBestand(int sollBestand) {
        this.sollBestand = sollBestand;
    }

    public int getIstBestand() {
        return istBestand;
    }

    public void setIstBestand(int istBestand) {
        this.istBestand = istBestand;
    }

    public void nachbestellen() {

        if ((double) istBestand < 0.8 * (double) sollBestand) {
            int benoetigteWare = sollBestand - istBestand;
            if (benoetigteWare > 1) {
                System.out.println(benoetigteWare + " Artikel werden nachbestellt!");
            } else {
                System.out.println(benoetigteWare + " Artikel wird nachbestellt!");
            }
            lagerbestandVeraendern(benoetigteWare);
            System.out.println("Neuer Bestand: " + istBestand);
        } else {
            System.out.println("Der Bestand ist ausreichend!");
        }

    }

    public void lagerbestandVeraendern(int anzahl)  {
        istBestand = istBestand + anzahl;
    }

    public double berechneMarge(){
        double marge = verkaufspreis - einkaufspreis;
        System.out.printf("Die Marge beträgt %.2f Euro.\n", marge);
        return marge;
    }
}

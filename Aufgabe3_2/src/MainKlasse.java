public class MainKlasse {
    public static void main(String args[]) {
        double mittelWert = berechneMittelwert(2,4);
        System.out.println(mittelWert);
        double widerstandReihe = reihenschaltung(100,200);
        System.out.println(widerstandReihe);
        double widerstandParallel = parallelschaltung(100,1000);
        System.out.println(widerstandParallel);
    }

    //Aufgabe 1
    public static double berechneMittelwert(double x, double y) {
        return (x+y)/2;
    }

    //Aufgabe 3
    public static double reihenschaltung(double r1, double r2){
        return r1+r2;
    }

    public static double parallelschaltung(double r1, double r2){
        return r1*r2/(r1+r2);
    }
}

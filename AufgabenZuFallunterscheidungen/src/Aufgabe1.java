import java.util.Scanner;

import javax.sound.midi.MidiEvent;

public class Aufgabe1 {

	public static void main(String[] args) {
		int note = notenEingabe();
		switch (note) {
		case 1:
			System.out.println("Die Note lautet 'sehr gut'!");
			break;
		case 2:
			System.out.println("Die Note lautet 'gut'!");
			break;
		case 3:
			System.out.println("Die Note lautet 'befriedigend!");
			break;
		case 4:
			System.out.println("Die Note lautet 'ausreichend'!");
			break;
		case 5:
			System.out.println("Die Note lautet 'mangelhaft'!");
			break;
		case 6:
			System.out.println("Die Note lautet 'ungenügend'!");
			break;
		default:
			System.out.println("Bitte eine Zahl zwischen 1-6 eingeben!");
			break;
		}


	}

	public static int notenEingabe() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Bitte ein Note zwischen 1 und 6 eingeben!");
		return scanner.nextInt();
	}

}

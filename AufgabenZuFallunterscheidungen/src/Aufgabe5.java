import java.util.Scanner;

public class Aufgabe5 {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		double spannung = 0.0;
		double strom = 0.0;
		double widerstand = 0.0;
		char zielgroesse = eingabeZuBerechnendeGroesse();
		switch (zielgroesse) {
		case 'U':
			strom = eingabeWert("Strom", "Ampere");
			widerstand = eingabeWert("Widerstand", "Ohm");
			spannung = strom * widerstand;
			ausgabe("Spannung", "Volt", spannung);
			break;
		case 'I':
			spannung = eingabeWert("Spannung", "Volt");
			widerstand = eingabeWert("Widerstand", "Ohm");
			if (widerstand == 0.0) {
				System.out.println("Fehler: Widerstand darf nicht 0 sein!");
			} else {
				strom = spannung / widerstand;
				ausgabe("Strom", "Ampere", strom);
			}
			break;
		case 'R':
			spannung = eingabeWert("Spannung", "Volt");
			strom = eingabeWert("Strom", "Ampere");
			if (strom == 0.0) {
				System.out.println("Fehler: Strom darf nicht 0 sein!");
			} else {
				widerstand = spannung / strom;
				ausgabe("Widerstand", "Ohm", widerstand);
			}
			break;
		default:
			System.out.println("Bitte einen der Buchstaben 'U', 'I' oder 'R' auswählen!");
		}

	}

	public static char eingabeZuBerechnendeGroesse() {
		System.out.println("Welche Größe Soll Berechnet werden? (U,I,R)");
		char groesse = scanner.next().charAt(0);
		return groesse;

	}

	public static double eingabeWert(String groesse, String einheit) {
		System.out.println("Bitte " + groesse + " in " + einheit + " eingeben:");
		double wert = scanner.nextDouble();
		return wert;
	}

	public static void ausgabe(String groesse, String einheit, double wert) {
		System.out.println(groesse + " beträgt: " + wert +" " + einheit);
	}

}

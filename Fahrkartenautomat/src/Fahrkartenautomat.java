import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		double eingeworfeneMuenze;
		double rueckgabebetrag;
		Scanner tastatur = new Scanner(System.in);

		while (true) {

			zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);

			// Geldeinwurf
			// -----------
			eingezahlterGesamtbetrag = fahrkartenBezahlen(tastatur, zuZahlenderBetrag);

			// Fahrscheinausgabe
			// -----------------

			fahrkartenAusgeben();

			// Rückgeldberechnung und -Ausgabe
			// -------------------------------
			rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
			if (rueckgabebetrag > 0.0) {
				rueckgeldAusgeben(rueckgabebetrag);
			}

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt.\n\n");
			System.out.println("----------------------------------------------------------\n\n\n");
		}

	}

	public static double fahrkartenbestellungErfassen(Scanner tastatur) {
		// Aufgabe 5.: Datentyp für die Anzahl ist int, da es nur eine ganzzahlige
		// Anzahl an Tickets gibt.
		int ticketArt = 0;
		int anzahlTickets;
		double ticketPreis = 0;
		boolean bestellungAbschliessen = false;
		boolean korrektesTicketAusgewaehlt;
		double zuZahlenderBetrag = 0;

		while (!bestellungAbschliessen) {
			korrektesTicketAusgewaehlt = false;
			
			if (zuZahlenderBetrag > 0) {
				System.out.println("\n\nWeitere tickets kaufen?\n");
			}

			System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
			System.out.println("\tEinzelfahrschein Regeltarif AB [2,90 EUR] (1)");
			System.out.println("\tTageskarte Regeltarif AB [8,60 EUR] (2)");
			System.out.println("\t Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
			System.out.println("\t Bezahlen (9)");

			while (!korrektesTicketAusgewaehlt) {

				System.out.print("\nIhre Wahl: ");

				korrektesTicketAusgewaehlt = true;
				ticketArt = tastatur.nextInt();

				switch (ticketArt) {
				case 1: {
					ticketPreis = 2.9;
					break;
				}
				case 2: {
					ticketPreis = 8.6;
					break;
				}
				case 3: {
					ticketPreis = 23.5;
					break;
				}
				case 9: {
					bestellungAbschliessen = true;
					break;
				}
				default: {
					System.out.print(">>falsche Eingabe<<");
					korrektesTicketAusgewaehlt = false;
				}
				}

			}

			if (!bestellungAbschliessen) {

				do {
					System.out.print("Anzahl der Tickets (maximal 10 Tickets): ");
					anzahlTickets = tastatur.nextInt();

					if (anzahlTickets > 10 || anzahlTickets < 1) {
						System.out.println(" >> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
					}
				} while (anzahlTickets > 10 || anzahlTickets < 1);

				zuZahlenderBetrag = zuZahlenderBetrag + ticketPreis * anzahlTickets;
			}
		}
		System.out.printf("Gesamtbetrag (EURO): %.2f\n\n", zuZahlenderBetrag);
		return zuZahlenderBetrag;

	}

	public static double fahrkartenBezahlen(Scanner tastatur, double zuZahlenderBetrag) {
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			double eingeworfeneMuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMuenze;
		}
		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double rueckgabebetrag) {
		System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", rueckgabebetrag);
		System.out.println("wird in folgenden Münzen ausgezahlt:");

		while (rueckgabebetrag >= 2.0) // 2 EURO-Münzen
		{
			muenzeAusgeben(2, "EURO");
			rueckgabebetrag -= 2.0;
		}
		while (rueckgabebetrag >= 1.0) // 1 EURO-Münzen
		{
			muenzeAusgeben(1, "EURO");
			rueckgabebetrag -= 1.0;
		}
		while (rueckgabebetrag >= 0.5) // 50 CENT-Münzen
		{
			muenzeAusgeben(50, "CENT");
			rueckgabebetrag -= 0.5;
		}
		while (rueckgabebetrag >= 0.2) // 20 CENT-Münzen
		{
			muenzeAusgeben(20, "CENT");
			rueckgabebetrag -= 0.2;
		}
		while (rueckgabebetrag >= 0.1) // 10 CENT-Münzen
		{
			muenzeAusgeben(10, "CENT");
			rueckgabebetrag -= 0.1;
		}
		// Aufgrund von Rundungsfehlern werden 5Cent Stücke nicht zurück gegeben!!!
		while (rueckgabebetrag >= 0.05)// 5 CENT-Münzen
		{
			muenzeAusgeben(5, "CENT");
			rueckgabebetrag -= 0.05;
		}
	}

	static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);
	}
}
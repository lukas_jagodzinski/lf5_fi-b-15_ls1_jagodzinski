import java.util.Scanner;

public class Fahrsimulator {
    public static void main(String args[]){
        double v = 0.0;

        do {
            System.out.println("Bitte Wert für Beschleunigug angeben:");
            Scanner myScanner = new Scanner(System.in);
            double dv = myScanner.nextDouble();
            v = beschleunige(v,dv);
            System.out.println("Geschwindigkeit: " + v);
        } while (v > 0.0);
        System.out.println("Das Fahrzeug steht");
    }

   public static double beschleunige(double v, double dv){
        double neueGeschwindigkeit = v + dv;
        if (neueGeschwindigkeit > 130.0) {
            return 130.0;
       } else if (neueGeschwindigkeit < 0.0) {
            return 0.0;
       }
       return neueGeschwindigkeit;
    }
}

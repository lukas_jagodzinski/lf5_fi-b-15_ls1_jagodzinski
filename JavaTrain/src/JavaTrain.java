import java.util.Scanner;

public class JavaTrain {

	public static void main(String[] args) {
		int fahrzeit = 0;
		char haltInSpandau = 'n';
		char richtungHamburg = 'n';
		char haltInStendal = 'n';
		char endetIn = 'h';
		int fahrtZiel = 0;

		while (fahrtZiel < 1 || fahrtZiel > 4) {
			fahrtZiel = fahrtZielAbfrage();
			if (fahrtZiel == 1) {
				richtungHamburg = 'j';
			} else if (fahrtZiel == 2) {
				richtungHamburg = 'n';
				endetIn = 'h';
			} else if (fahrtZiel == 3) {
				richtungHamburg = 'n';
				endetIn = 'w';
			} else if (fahrtZiel == 4) {
				richtungHamburg = 'n';
				endetIn = 'b';
			} else {
				System.out.println("Bitte korrekte Stadt auswählen (1-4)!");
			}
		}

		fahrzeit = fahrzeit + 8; // Fahrzeit: Berlin Hbf -> Spandau

		if (haltInSpandau == 'j') {
			fahrzeit = fahrzeit + 2; // Halt in Spandau
		}
		if (richtungHamburg == 'j') {
			fahrzeit = fahrzeit + 96;
			ausgabeAnkunft("Hamburg", fahrzeit);
		} else {
			fahrzeit = fahrzeit + 34;
			if (haltInStendal == 'j') {
				fahrzeit += 16;
			} else {
				fahrzeit += 6;
			}
			if (endetIn == 'w') {
				fahrzeit += 29;
				ausgabeAnkunft("Wolfsburg", fahrzeit);
			} else if (endetIn == 'b') {
				fahrzeit += 50;
				ausgabeAnkunft("Braunschweig", fahrzeit);
			} else {
				fahrzeit += 62;
				ausgabeAnkunft("Hannover", fahrzeit);
			}
		}

	}

	public static int fahrtZielAbfrage() {
		int ziel = 0;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Wohin möchten sie fahren?");
		System.out.println("1. Hamburg");
		System.out.println("2. Hannover");
		System.out.println("3. Wolfsburg");
		System.out.println("4. Braunschweig\n");
		try {
			ziel = scanner.nextInt();
		} catch (Exception e) {
			System.out.println("Bitte eine ganze Zahl im Bereich 1-4 eingeben!");
		}
		return ziel;
	}

	public static void ausgabeAnkunft(String stadt, int fahrzeit) {
		System.out.println("Sie erreichen " + stadt + " nach " + fahrzeit + " Minuten.");
	}

}


public class Konsolenausgabe1 {
	public static void aufgabe1() {
		
		int i = 1;
		int j = 2;
		System.out.print("Das ist mein " + i + ". Satz.\n");
		System.out.print("Und das ist der " + j + ". \"Satz\".");
		 
		//Das ist ein Kommentar
	}
	
	public static void aufgabe2() {
		System.out.println("\n      *      ");
		System.out.println("     ***     ");
		System.out.println("    *****    ");
		System.out.println("   *******   ");
		System.out.println("  *********  ");
		System.out.println(" *********** ");
		System.out.println("*************");
		System.out.println("     ***     ");
		System.out.println("     ***     ");
	}
	
	public static void aufgabe3() {
		double zahl1 = 22.4234234;
		double zahl2 = 111.2222;
		double zahl3 = 4.0;
		double zahl4 = 1000000.551;
		double zahl5 = 97.34;
		
		System.out.printf( "%.2f\n" , zahl1);
		System.out.printf( "%.2f\n" , zahl2);
		System.out.printf( "%.2f\n" , zahl3);
		System.out.printf( "%.2f\n" , zahl4);
		System.out.printf( "%.2f\n" , zahl5);
	}
}

public class Konsolenausgabe2 {

    public static void aufgabe1(){
        System.out.println("   **");
        System.out.println("*      *");
        System.out.println("*      *");
        System.out.println("   **\n\n");
    }

    public static void aufgabe2(){
        System.out.printf("%-5s", "0!");
        System.out.printf("= ");
        System.out.printf("%-19s", "");
        System.out.printf("=");
        System.out.printf("%4s\n", "1");

        System.out.printf("%-5s", "1!");
        System.out.printf("= ");
        System.out.printf("%-19s", "1");
        System.out.printf("=");
        System.out.printf("%4s\n", "1");

        System.out.printf("%-5s", "2!");
        System.out.printf("= ");
        System.out.printf("%-19s", "1 * 2");
        System.out.printf("=");
        System.out.printf("%4s\n", "2");

        System.out.printf("%-5s", "3!");
        System.out.printf("= ");
        System.out.printf("%-19s", "1 * 2 * 3");
        System.out.printf("=");
        System.out.printf("%4s\n", "6");

        System.out.printf("%-5s", "4!");
        System.out.printf("= ");
        System.out.printf("%-19s", "1 * 2 * 3 * 4");
        System.out.printf("=");
        System.out.printf("%4s\n", "24");

        System.out.printf("%-5s", "5!");
        System.out.printf("= ");
        System.out.printf("%-19s", "1 * 2 * 3 * 4 * 5");
        System.out.printf("=");
        System.out.printf("%4s\n\n", "120");
    }
    
    public static void aufgabe3() {
    	System.out.printf("%-12s|", "Fahrenheit");
    	System.out.printf("%10s\n", "Celsius");
    	
    	System.out.println("-----------------------");
    	
    	System.out.printf("%+-12d|", -20);
    	System.out.printf("%10.2f\n", -28.8889);
    	
    	System.out.printf("%+-12d|", -10);
    	System.out.printf("%10.2f\n", -23.3333);
    	
    	System.out.printf("%+-12d|", 0);
    	System.out.printf("%10.2f\n", -17.7778);
    	
    	System.out.printf("%+-12d|", 20);
    	System.out.printf("%10.2f\n", -6.6667);
    	
    	System.out.printf("%+-12d|", 0);
    	System.out.printf("%10.2f\n", -1.1111);
    	
    }
}

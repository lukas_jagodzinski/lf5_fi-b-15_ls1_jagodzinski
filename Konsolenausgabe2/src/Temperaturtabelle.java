
public class Temperaturtabelle {
	
	public static void main(String[] args) {
		
	  	System.out.printf("%-12s|", "Fahrenheit");
    	System.out.printf("%10s\n", "Celsius");
    	
    	System.out.println("-----------------------");
    	
    	System.out.printf("%+-12d|", -20);
    	System.out.printf("%10.2f\n", -28.8889);
    	
    	System.out.printf("%+-12d|", -10);
    	System.out.printf("%10.2f\n", -23.3333);
    	
    	System.out.printf("%+-12d|", 0);
    	System.out.printf("%10.2f\n", -17.7778);
    	
    	System.out.printf("%+-12d|", 20);
    	System.out.printf("%10.2f\n", -6.6667);
    	
    	System.out.printf("%+-12d|", 0);
    	System.out.printf("%10.2f\n", -1.1111);
    	
	}

}

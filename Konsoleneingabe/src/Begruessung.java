import java.util.Scanner;

public class Begruessung {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Hallo Benutzer! Wie lautet dein Name?");
		String name = scanner.next();

		System.out.println("Wie alt bist du?");
		int alter = scanner.nextInt();

		System.out.println("Dein Name lautet " + name + ". Du bist " + alter + " Jahre alt");
		scanner.close();
	}

}

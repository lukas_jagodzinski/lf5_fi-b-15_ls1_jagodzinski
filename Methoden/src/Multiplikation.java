public class Multiplikation {

public static void main(String[] args) {

	double zahl1 = 2.36;
	double zahl2 = 7.87;
	
	titel(zahl1, zahl2);

	double ergebnis = verarbeitung(zahl1, zahl2);
	
	ausgabe(ergebnis);
	
	

	}	


	public static void titel(double zahl1, double zahl2) {
		System.out.println("Dieses Programm berechnet das Produkt aus " + zahl1 + " und " + zahl2 + ".\n");
	}
	
	public static void ausgabe(double ergebnis) {
		System.out.println("Das Ergebnis lautet: " + ergebnis);
	}
	
	public static double verarbeitung(double zahl1, double zahl2) {
		return zahl1 * zahl2;
	}
}

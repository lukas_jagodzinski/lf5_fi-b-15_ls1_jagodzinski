import java.util.Scanner;

public class Volumen {
	   
		public static void main(String[] args) {

			// (E) "Eingabe"
			// Wert für x festlegen:
			// ===========================
			titel();
			
			System.out.println("Eingabe Abmessungen Würfel");
			double kanteWuerfel = eingabe("Bitte die Kantenlänge des Würfels eingeben: ");
			
			System.out.println("\nEingabe Abmessungen Quader");
			double hoeheQuader = eingabe("Bitte die Höhe des Quaders eingeben: ");
			double laengeQuader = eingabe("Bitte die Länge des Quaders eingeben: ");
			double breiteQuader = eingabe("Bitte die Breite des Quaders eingeben: ");
			
			
			System.out.println("\nEingabe Abmessungen Pyramide");
			double hoehePyramide = eingabe("Bitte die Höhe der Pyramide eingeben: ");
			double seitePyramide = eingabe("Bitte die Seitenlänge der Pyramide eingeben: ");
			
			System.out.println("\nEingabe Abmessungen Kugel");
			double radiusKugel = eingabe("Bitte den Radius der Kugel eingeben: \n\n");

			
			
			// (V) Verarbeitung
			// Mittelwert von x und y berechnen:
			// ================================
			double volumenWuerfel = berechnungWuerfel(kanteWuerfel);
			double volumenQuader = berechnungQuader(hoeheQuader, breiteQuader, laengeQuader);
			double volumenPyramide = berechnungPyramide(seitePyramide, hoehePyramide);
			double volumenKugel = berechnungKugel(radiusKugel);
			
		

			// (A) Ausgabe
			// Ergebnis auf der Konsole ausgeben:
			// =================================
			ausgabe("Würfel", volumenWuerfel);
			ausgabe("Quader", volumenQuader);
			ausgabe("Pyramide", volumenPyramide);
			ausgabe("Kugel", volumenKugel);
		}	
		
		public static double eingabe(String anweisung) {
			System.out.print(anweisung);
			Scanner scanner = new Scanner(System.in);
			return scanner.nextDouble();
		}
		
		public static void titel() {
			System.out.println("Dieses Programm berechnet Volumen verschiedener geometrischer Körper");
			System.out.println("--------------------------------------------------------------------");
		}

		public static double berechnungWuerfel(double a) {
			return  a * a * a;
		}
		public static double berechnungQuader(double a,double b,double c) {
			return  a * b * c;
		}
		
		public static double berechnungPyramide(double a,double h) {
			return  a * a * h / 3;
		}
		
		public static double berechnungKugel(double r) {
			double pi = 3.14;
			return   r * r * r * pi * 4 / 3;
		}
		
		public static void ausgabe(String koerper, double volumen) {
			System.out.println("Volumen " + koerper + ": " + volumen);	
		}
		

}

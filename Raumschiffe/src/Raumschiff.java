import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {

    private int photonentorpedoAnzahl;
    private int energieversorgungInProzent;
    private int zustandSchildeInProzent;
    private int zustandHuelleInProzent;
    private int zustandLebenserhaltungssystemeInProzent;
    private int androidenAnzahl;
    private String schiffsname;
    private static ArrayList<String> broadcastKommunikator;
    private ArrayList<Ladung> ladungsverzeichnis;


    public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
        this.energieversorgungInProzent = energieversorgungInProzent;
        this.zustandSchildeInProzent = schildeInProzent;
        this.zustandHuelleInProzent = huelleInProzent;
        this.zustandLebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
        this.androidenAnzahl = androidenAnzahl;
        this.schiffsname = schiffsname;
        if (this.broadcastKommunikator == null) {
            this.broadcastKommunikator = new ArrayList<>();
        }
        this.ladungsverzeichnis = new ArrayList<>();
    }

    public Raumschiff() {
        if (this.broadcastKommunikator == null) {
            this.broadcastKommunikator = new ArrayList<>();
        }
        this.ladungsverzeichnis = new ArrayList<>();
    }

    public int getPhotonentorpedoAnzahl() {
        return this.photonentorpedoAnzahl;
    }

    public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
    }

    public int getEnergieversorgungInProzent() {
        return energieversorgungInProzent;
    }

    public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
        this.energieversorgungInProzent = energieversorgungInProzent;
    }

    public int getZustandSchildeInProzent() {
        return zustandSchildeInProzent;
    }

    public void setZustandSchildeInProzent(int zustandSchildeInProzent) {
        this.zustandSchildeInProzent = zustandSchildeInProzent;
    }

    public int getZustandHuelleInProzent() {
        return zustandHuelleInProzent;
    }

    public void setZustandHuelleInProzent(int zustandHuelleInProzent) {
        this.zustandHuelleInProzent = zustandHuelleInProzent;
    }

    public int getZustandLebenserhaltungssystemeInProzent() {
        return zustandLebenserhaltungssystemeInProzent;
    }

    public void setZustandLebenserhaltungssystemeInProzent(int zustandLebenserhaltungssystemeInProzent) {
        this.zustandLebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
    }

    public int getAndroidenAnzahl() {
        return androidenAnzahl;
    }

    public void setAndroidenAnzahl(int androidenAnzahl) {
        this.androidenAnzahl = androidenAnzahl;
    }

    public String getSchiffsname() {
        return schiffsname;
    }

    public void setSchiffsname(String schiffsname) {
        this.schiffsname = schiffsname;
    }

    //    Es ist nicht im OOD aufgeführt, aber es gibt ansonsten keine Möglichkeit das Ladungsverzeichnis abzurufen.
    public ArrayList<Ladung> getLadungsverzeichnis() {
        return ladungsverzeichnis;
    }

    public void addLadung(Ladung neueLadung) {
        this.ladungsverzeichnis.add(neueLadung);
    }

    public void photonentorpedoSchiessen(Raumschiff r) {
        if (photonentorpedoAnzahl > 0) {
            photonentorpedoAnzahl = photonentorpedoAnzahl - 1;
            broadcastKommunikator.add("Photonentorpedo abgeschossen");
            treffer(r);
        } else {
            broadcastKommunikator.add("-=*Click*=-");
        }
    }

    public void phaserkanoneSchiessen(Raumschiff r) {
        if (energieversorgungInProzent < 50) {
            broadcastKommunikator.add("-=*Click*=-");
        } else {
            energieversorgungInProzent = energieversorgungInProzent / 2;
            broadcastKommunikator.add("Phaserkanone abgeschossen");
            treffer(r);
        }
    }

    private void treffer(Raumschiff r) {
        System.out.println(r.getSchiffsname() + " wurde getroffen!");
        r.setZustandSchildeInProzent(r.getZustandSchildeInProzent() / 2);
        if (r.getZustandSchildeInProzent() == 0) {
            r.setZustandHuelleInProzent(r.getZustandHuelleInProzent() / 2);
            r.setEnergieversorgungInProzent(r.getEnergieversorgungInProzent() / 2);
            if (r.getZustandHuelleInProzent() == 0) {
                r.setZustandLebenserhaltungssystemeInProzent(0);
                broadcastKommunikator.add("Lebenserhaltungssysteme von Raumschiff " + r.getSchiffsname() + " wurden zerstört!");
            }
        }
    }

    public void nachrichtAnAlle(String message) {
        broadcastKommunikator.add(message);
    }

    public static ArrayList<String> eintraegeLogbuchZurueckgeben() {
        return broadcastKommunikator;
    }

    public void photonenTorpedosEinsetzen(int anzahl) {
        boolean photonenTorpedosGeladen = false;
        for (Ladung ladung : ladungsverzeichnis) {
            if (ladung.getBezeichnung().equals("Photonentorpedo")) {
                if (ladung.getMenge() >= anzahl) {
                    photonentorpedoAnzahl = photonentorpedoAnzahl + anzahl;
                    ladung.setMenge(ladung.getMenge() - anzahl);
                } else {
                    photonentorpedoAnzahl = photonentorpedoAnzahl + ladung.getMenge();
                    ladung.setMenge(0);
                }
                break;
            }
        }

    }


    public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
        int anzahlReparaturen = 0;
        if (schutzschilde) {
            anzahlReparaturen++;
        }
        if (energieversorgung) {
            anzahlReparaturen++;
        }
        if (schiffshuelle) {
            anzahlReparaturen++;
        }
        if (anzahlReparaturen > 0) {
            int anzahlEingesetzterDroiden = 0;
            if (androidenAnzahl >= anzahlDroiden) {
                anzahlEingesetzterDroiden = anzahlDroiden;
            } else {
                anzahlEingesetzterDroiden = androidenAnzahl;
            }
            if (anzahlEingesetzterDroiden > 0) {
                double randomNumber = new Random().nextDouble() * 100;
                int reparaturInProzent = (int) Math.round(randomNumber * anzahlEingesetzterDroiden / anzahlReparaturen);
                if (schutzschilde) {
                    zustandSchildeInProzent += reparaturInProzent;
                    if (zustandSchildeInProzent > 100) {
                        zustandSchildeInProzent = 100;
                    }
                }
                if (energieversorgung) {
                    energieversorgungInProzent += reparaturInProzent;
                    if (energieversorgungInProzent > 100) {
                        energieversorgungInProzent = 100;
                    }
                }
                if (schiffshuelle) {
                    zustandHuelleInProzent += reparaturInProzent;
                    if (zustandHuelleInProzent > 100) {
                        zustandHuelleInProzent = 100;
                    }
                }
            }


        }

    }

    public void zustandRaumschiff() {
        System.out.println("Zustand Raumschiff " + schiffsname);
        System.out.println("------------------");
        System.out.println("Zustand Schilde: " + zustandSchildeInProzent);
        System.out.println("Zustand Hülle: " + zustandHuelleInProzent);
        System.out.println("Zustand Lebenserhaltungssysteme: " + zustandLebenserhaltungssystemeInProzent);
        System.out.println("Zustand Energieversorgung: " + energieversorgungInProzent);

        System.out.println("---------------------");

    }

    public void ladungsverzeichnisAusgeben() {
        System.out.println("Ladungsverzeichnis Raumschiff " + schiffsname);
        System.out.println("------------------");
        for (Ladung ladung : ladungsverzeichnis) {
            System.out.println(ladung);
        }
        System.out.println("\n---------------------");
        System.out.println("Ende Ladungsverzeichnis" + schiffsname);
    }

    public void ladungsverzeichnisAufraeumen() {
        ladungsverzeichnis.removeIf(ladung -> ladung.getMenge() == 0);
    }

    public static void broadcastAufKonsole() {
        if (broadcastKommunikator != null) {
            System.out.println("Start Broadcastcommunicatorausgabe:");
            System.out.println("-----------------------------------");
            for (String message : broadcastKommunikator) {
                System.out.println(message);
            }
            System.out.println("----------------------------------");
            System.out.println("Ende Broadcastcommunicatorausgabe:");
        } else {
         System.out.println("Der Broadcastcommunicator existiert nicht!");
        }
    }

    @Override
    public String toString() {
        return "Schiffsname: " + schiffsname +
                "\nAnzahl Photonentorpedos: " + photonentorpedoAnzahl +
                "\nEnergieversorgung in Prozent:" + energieversorgungInProzent +
                "\nZustand Schilde in Prozent: " + zustandSchildeInProzent +
                "\nZustand Hülle in Prozent: " + zustandHuelleInProzent +
                "\nZustand Lebenserhaltungssysteme in Prozent: " + zustandLebenserhaltungssystemeInProzent +
                "\nAnzahl Droiden: " + androidenAnzahl +
                "\nLadungsverzeichnis=" + ladungsverzeichnis + "\n";
    }

}

/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Lukas Jagodzinski >>
  */
  
public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten =  8;
    
    // Anzahl der Sterne in unserer Milchstraße
     long   anzahlSterne = 200000000000l;
    
    // Wie viele Einwohner hat Berlin?
      int  bewohnerBerlin = 3766082;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
       int alterTage = 15476;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
       int gewichtKilogramm =  140000; 
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
       int flaecheGroessteLand = 17130000;
    
    // Wie groß ist das kleinste Land der Erde?
    
       double flaecheKleinsteLand = 0.44;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    System.out.println("Einwohner Berlins: " + bewohnerBerlin);
    System.out.println("Mein Alter in Tagen: " + alterTage);
    System.out.println("Gewicht des Blauwals in kg: " + gewichtKilogramm);
    System.out.println("Fläche Russlands in km²: " + flaecheGroessteLand);
    System.out.println("Fläche Russlands in km²: " + flaecheKleinsteLand);
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}
